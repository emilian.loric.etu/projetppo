package solveur;
import java.util.*;

public class Station {
	Map<Integer,Point> points = new HashMap<Integer,Point>();
	
	Map<Integer,Boolean> mark = new HashMap<Integer,Boolean>();
	Map<Integer,Double> pi = new HashMap<Integer,Double>();
	Map<Integer,Transition> pere = new HashMap<Integer,Transition>();
	
	
	protected void dijkstra(Point ptDepart, Point ptArrivee) {
		
		
		int k,i;
		this.addPi(ptDepart.getId(),0.0);
		this.addPe(ptDepart.getId(),new Navette()); //transition
		this.addM(0,false);
		Point x = null;
		double minweight = 9999999;
		int minweightID = -1;
		int idx = -1;
		
		while(mark.get(ptArrivee.getId()) == false){
			//Choisir le point x tel que le le point soit non marqué et le poids minimal
			int nbPoints = this.points.size();
			for(i=0;i<nbPoints-1;i++) {
				x = points.get(i);
				idx = x.getId();
				
				if(mark.get(idx) == false && pi.get(idx) < minweight) {
					minweight = pi.get(idx);
					//Enregistrer l'id du point pour connare le point courant sélectionné
					minweightID = idx;
				}
			}
			//Il y a un point sur lequel aller si (au minimum) un point n'est pas marqué !!
			if (minweightID >= 0) {
				this.setMark(minweightID,true);
				int nbTransition = x.pointTransitions.size();
				
				for(k=0;k<nbTransition-1;i++) {
					int y = transixTok(x,k).getId();
					addM(y, false);
					addPi(y, transixTok(x,k).getTpsTraj());
			
					double newweight = pi.get(idx)+points.get(idx).pointTransitions.get(k).getTpsTraj();
					if(newweight < pi.get(y)) {
						pi.replace(y,newweight);
						addPe(idx, transixTok(x,k));
					}
				}
			}
		}
	}
		
	private Transition transixTok(Point x, int k) {
		return x.getPointTransitions().get(k);
	}
	public void addPi(Integer cle, Double w) {
		pi.put(cle,w);
	}
	public void addPe(Integer cle, Transition t) {
		pere.put(cle,t);
	}
	public void addM(Integer cle, boolean b) {
		mark.put(cle,b);
	}

	
	
	
	protected void setMark(Integer id, boolean bo) {
		if(mark.containsKey(id)) {
			this.mark.replace(id, bo);
		}else {
			this.mark.put(id, bo);
		}
	}

	
}
